
package Controlador;

import Mysql.MySQL;
import Vista.Menu;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;



public class MenuController {
    
    Connection conn;
    
    Menu interfaz;
    public MenuController( Menu GUI){
         
       interfaz=GUI;
       conn= MySQL.getConexion();
    }
    
    public void borrartable(){
        PreparedStatement stmt = null;
        //int tuplas = 0;
        try {
            stmt = conn.prepareStatement("delete from Movimientos where ID != 0;");
            int tuplas = stmt.executeUpdate();
            //rs.close();
            stmt.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }
}
